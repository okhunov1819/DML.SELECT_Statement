WITH ActorRoleGaps AS (
    SELECT 
        a.actor_id,
        a.first_name || ' ' || a.last_name AS actor_name,
        MAX(f.release_year) - MIN(f.release_year) AS role_gap_years
    FROM 
        actor a
    JOIN 
        film_actor fa ON a.actor_id = fa.actor_id
    JOIN 
        film f ON fa.film_id = f.film_id
    GROUP BY 
        a.actor_id
    ORDER BY 
        role_gap_years DESC
    LIMIT 1
)
SELECT 
    actor_name,
    role_gap_years
FROM 
    ActorRoleGaps;